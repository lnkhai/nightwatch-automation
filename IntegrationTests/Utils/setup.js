import { log } from 'utils';
import config from 'config';

export function before(browser) {
  browser.deleteCookies();
  browser.maximizeWindow();
  browser.init(config.test_settings.default.launch_url);

  browser.init(config.test_settings.default.launch_url);
}

export function after(browser) {
  browser.end();
}
