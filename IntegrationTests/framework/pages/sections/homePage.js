export default {
  selector: '#quotebox',
  elements: {
    carInsuranceQuote: { selector: '#getcarquote' },
    lifeInsuranceQuote: { selector: '#getlifequote' }
  },
};
