import { lifeInsuranceQuote } from './sections';
import config from 'config';

export const sections = {
    lifeInsuranceQuote
};


// Profile   |  age |  gender  |  state             | make             |  premium  |
// Profile 1 |  23  |  male    |  New South Wales   | BMW              |  $55.54   |
// Profile 2 |  70  |  female  |  Victoria          | Alfa Romeo       |  $137.51  |
// Profile 3 |  100 |  female  |  Queensland        | Audi             |  $230.8   |
// Profile 4 |  18  |  female  |  Western Australia | BMW              |  $51.73   |

export const commands = [{
  getLifeInsuranceQuote: function () {
    this.section.homePage.clickAt('@carInsuranceQuote');
    
    return this;
  },

  test2: function () {
    this.section.homePage.clickAt('@carInsuranceQuote');
    
    return this;
  },
}];
