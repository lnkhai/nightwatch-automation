import { homePage } from 'sections';
import { log } from 'utils';

export const sections = {
  homePage
};

export const commands = [{
  showCarInsuranceQuote: function () {
    this.section.homePage.clickAt('@carInsuranceQuote');
    
    return this;
  },

  showLifeInsuranceQuote: function () {
    this.section.homePage.clickAt('@lifeInsuranceQuote');

    return this;
  }
  
}];
