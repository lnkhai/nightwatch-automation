export default () => {
  return {
      profiles: {
        profile: {
            id: 1,
            age: '23',
            gender: 'male',
            occupationCategory: 'High Risk',
            state: 'New South Wales',
            email: 'lifeProfile1@gmail.com'
        }

      }
  };
};