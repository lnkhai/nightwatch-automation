export default () => {
  return {
    age: '75',
    gender: 'male',
    occupationCategory: 'Very High Risk',
    state: 'Tasmania',
    email: 'lifeProfile2@gmail.com'
  };
};
// Premium base on pre-defined ranks for Life Insurance
// Profile   |  age |  gender  |  state             | occupation       |  premium  |
// Profile 1 |  23  |  male    |  New South Wales   | High Risk        |  $55.54   |
// Profile 2 |  75  |  male    |  Tasmania          | Very High Risk   |  $229.38  |
// Profile 3 |  100 |  female  |  Queensland        | Low Risk         |  $230.8   |
// Profile 4 |  18  |  female  |  Western Australia | High Risk        |  $51.73   |