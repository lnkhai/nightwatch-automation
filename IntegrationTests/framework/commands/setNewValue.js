export function command(selector, value) {
  this.waitForElementVisible(selector, 20000);
  this.clearValue(selector);
  this.setValue(selector, value);

  return this;
}

