export function command(selector) {
  this.waitForElementPresent(selector);
  this.click(selector);

  return this;
}
