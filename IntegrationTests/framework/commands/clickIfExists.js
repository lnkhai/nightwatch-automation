import { log } from 'utils';
import _ from 'lodash';

export function command(elements, callback) {
  const client = this;
  const elementSelector = elements[1].selector;
  const idPrefix = '#';
  if (!_.startsWith(elementSelector, idPrefix)) {
    throw new Error('Can not perform clickIfExists on non-id selectors. Selector must be started with ' + idPrefix + '.');
  }

  client.execute((selector) => {
    const fireClick = (node) => {
      const eventName = 'click';
      let doc;
      if (node.ownerDocument) {
        doc = node.ownerDocument;
      } else if (node.nodeType == 9) {
        doc = node;
      } else {
        throw new Error('Invalid node passed to fireEvent: ' + node.id);
      }

      if (node.dispatchEvent) {
        // Gecko-style approach (now the standard) takes more work
        const eventClass = 'MouseEvents';
        const event = doc.createEvent(eventClass);
        event.initEvent(eventName, true, true); // Event created as bubbling and cancelable.

        event.synthetic = true; // allow detection of synthetic events
        // The second parameter says go ahead with the default action
        node.dispatchEvent(event, true);
      } else if (node.fireEvent) {
        // IE-old school style
        const event = doc.createEventObject();
        event.synthetic = true; // allow detection of synthetic events
        node.fireEvent('on' + eventName, event);
      }
    };

    const element = document.getElementById(selector);

    if (element === null) {
      return false;
    }

    fireClick(element);

    return true;
  }, [_.trimStart(elementSelector, idPrefix)], result => {
    if (result.value) {
      log(`Element <${elementSelector}> was found and Event <click> fired.`);
    }
    if (typeof callback === 'function') {
      callback.call(client, result);
    }
  });

  return this;
}
