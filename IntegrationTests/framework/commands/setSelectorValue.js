export function command(selector, value) {
  this.waitForElementPresent(selector);
  this.setValue(selector, value);

  return this;
}
