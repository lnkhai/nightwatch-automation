module.exports = (function (config) {
  config.test_settings.default.launch_url = process.argv[2] || config.test_settings.default.launch_url;

  return config;
})(require('./nightwatch.json'));
