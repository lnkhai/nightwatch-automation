export { before, after } from 'utils';
import { log } from 'utils';

export function getLifeInsuranceQuoteAndDoPayment(browser) {
  browser
    .perform(async (client, done) => {
      var homePage = client.page.homePage();
      
      await homePage.showLifeInsuranceQuote();
      
      done();
    })
}
