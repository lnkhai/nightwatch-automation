require('babel-core/register');
require('babel-polyfill');
const del = require('del');

del(['./selenium-debug.log', './reports', './screenshots'])
  .then(() => {
    const nightwatch = require('nightwatch');

    const env = 'default';

    nightwatch.runner({
      config: './nightwatch.conf.js',
      env: env,
    }, () => {
    });
  }
);
